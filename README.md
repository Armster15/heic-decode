# `heic-decode`
This is a modified version of [`heic-decode`](https://github.com/catdad-experiments/heic-decode).

The problem was that Vite simply refused to bundle `libheif-js`, due to the following line:
```js
FS.genericErrors[code].stack = "<generic error, no stack>";
```

The modified version comments this line out and in addition, also is ESM, instead of CommonJS. This isn't the best solution but its the only one I currently have.

[The issue can be viewed here](https://github.com/catdad-experiments/libheif-js/issues/12)

This project is licensed using the same license as the parent repository.

