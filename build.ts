import fetch from "node-fetch";
import { format } from "prettier";
import { transform } from "esbuild";
import fs from "fs/promises";

/**
 * Inserts a string in a given index of another string
 * Example: insertAt("I want apple", " an", 6) => "I want an apple"
 */
const insertAt = (originalString: string, whatToAdd: string, index: number) => {
  var output = [originalString.slice(0, index), whatToAdd, originalString.slice(index)].join('');
  return output;
}

/**
 * Builds `libheif.esm.js`
 * Steps:
 * 1. Fetch `libheif.min.js` from JSDelivr
 * 2. Convert script from CJS to ESM using esbuild
 * 3. Format script using Prettier
 * 4. Comment out 'FS.genericErrors[code].stack = "<generic error, no stack>"'
 */
(async () => {
  // 1. Fetch libheif
  console.log("1. Fetching libheif...");
  const res = await fetch(
    "https://cdn.jsdelivr.net/npm/libheif-js/libheif/libheif.min.js"
  );
  const script = await res.text();


  // 2. Convert script from CJS to ESM using esbuild
  console.log("2. Using esbuild to convert the file from CJS to ESM")
  const { code: esmScript } = await transform(script, {
    format: "esm"
  });


  // 3. Format the script with Prettier
  console.log("3. Formatting script...");
  const formattedScript = format(esmScript, { parser: "babel" });


  // 4. Split script into array of lines and then
  // comment out the line:
  // `FS.genericErrors[code].stack = "<generic error, no stack>";`
  console.log(`4. Commenting out 'FS.genericErrors[code].stack = "<generic error, no stack>"'`)
  const scriptAsArray = formattedScript.split("\n");
  let index = scriptAsArray.findIndex((v) =>
    v.includes(`FS.genericErrors[code].stack = "<generic error, no stack>"`)
  );

  // Adds a "//" before the word "FS"
  scriptAsArray[index] = insertAt(scriptAsArray[index], "// ", scriptAsArray[index].indexOf("FS"))
  

  // 5. Output a `libhef.esm.js` file
  console.log("5. Outputting `libhef.esm.js`...")
  await fs.writeFile("libheif.esm.js", scriptAsArray.join("\n"));
})();
